/* trapezoid.c -- Serial Trapezoidal Rule
 *
 * Input: None.
 * Output:  Estimate of the integral from a to b of f(x)
 *    using the trapezoidal rule and n trapezoids.
 *
 *
 * Notes:  
 *    1.  First parameter is a, the left end of the range, and 
 *        the second parameter is b, the right end of the range. 
 *        If not there the program exits.
 *    2.  f(x) and n are hardwired.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

main(int argc, char** argv) {
    double      a;         /* Left endpoint default     */
    double      b;         /* Right endpoint default    */
    long int    n;         /* Number of trapezoids      */
    double      h;         /* Trapezoid base length     */
    double      integral;  /* Integral over my interval */

    struct timeval      t1,t2;
    long elapsed_useconds;    /* elapsed time in microseconds */
    long elapsed_mtime;    /* elapsed time in milliseconds */
    long elapsed_seconds;  /* diff between seconds counter */

    double Trap(double a, double b, long int n, double h);    /* Calculate local integral  */

    if (argc != 4) {
		printf("Usage:  trapezoid <left> <right> <trap_num>\n");
        exit(0);
    }

    gettimeofday(&t1, NULL);  /* Walltime */

    /* get the endpoints and calculate local variables */
    a = atof(argv[1]);               /*  left endpoint */
    b = atof(argv[2]);               /* right endpoint */
    n = atof(argv[3]);               /* Number of trapezoids */
    h = (b-a)/n;    /* h is the same for all processes */
    
    printf("\na=%f, b=%f, Local number of trapezoids=%d\n", a, b, n );

    integral = Trap(a, b, n, h);
   
    gettimeofday(&t2, NULL);  /* Walltime */
 
    elapsed_seconds  = t2.tv_sec  - t1.tv_sec;
    elapsed_useconds = t2.tv_usec - t1.tv_usec;
    elapsed_mtime = ((elapsed_seconds) * 1000 + elapsed_useconds/1000.0) + 0.5;
    
    if (elapsed_useconds < 0) {
		elapsed_useconds = 1000000 + elapsed_useconds;
		elapsed_seconds--;
    }
    printf("With n = %d trapezoids, our estimate\n", n);
    printf("of the integral from %f to %f = %f\n", a, b, integral);
    printf("\nElapsed time is %ld msec\n\n", elapsed_mtime);
    printf("\nElapsed time is %ld.%.6ld sec\n\n", elapsed_seconds, elapsed_useconds);

} /*  main  */

double Trap(double  a, double  b, long int  n, double  h) {
    double integral;   /* Store result in integral  */
    double x;
    long int i;

    double f(double x); /* function we're integrating */

    integral = (f(a) + f(b))/2.0;
    x = a;
    for (i = 1; i <= n-1; i++) {
        x = x + h;
        integral = integral + f(x);
    }
    integral = integral*h;
    return integral;
} /*  Trap  */

double f(double x) {
    double return_val;
    /* Calculate f(x). */
    /* Store calculation in return_val. */
    return_val = x*x;
    return return_val;
} /* f */


