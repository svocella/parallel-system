/* trap.c -- Parallel Trapezoidal Rule
 *
 * Input: None.
 * Output:  Estimate of the integral from a to b of f(x)
 *    using the trapezoidal rule and n trapezoids.
 *
 * Algorithm:
 *    1.  Each process calculates "its" interval of
 *        integration.
 *    2.  Each process estimates the integral of f(x)
 *        over its interval using the trapezoidal rule.
 *    3a. Each process != 0 sends its integral to 0.
 *    3b. Process 0 sums the calculations received from
 *        the individual processes and prints the result.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

main(int argc, char** argv) {
	double      a;         /* Left endpoint default     */
    double      b;         /* Right endpoint default    */
    long int    n;         /* Number of trapezoids      */
    double      h;         /* Trapezoid base length     */
    double      integral;  /* Integral over my interval */
	
    int         my_rank;   /* My process rank           */
    int         p;         /* The number of processes   */
    
	float       local_a;   /* Left endpoint my process  */
    float       local_b;   /* Right endpoint my process */
    int         local_n;   /* Number of trapezoids for  */
                           /* my calculation            */
						   
    float       total;     /* Total integral            */
    int         source;    /* Process sending integral  */
    int         dest = 0;  /* All messages go to 0      */
    int         tag = 0;
    MPI_Status  status;
	
	struct timeval      t1,t2;
    long elapsed_useconds;    /* elapsed time in microseconds */
    long elapsed_mtime;    /* elapsed time in milliseconds */
    long elapsed_seconds;  /* diff between seconds counter */

    float Trap(float a, float b, long int n, float h);    /* Calculate local integral  */

    /* Let the system do what it needs to start up MPI */
    MPI_Init(&argc, &argv);

    /* Get my process rank */
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    /* Find out how many processes are being used */
    MPI_Comm_size(MPI_COMM_WORLD, &p);
	
	gettimeofday(&t1, NULL);  /* Walltime */

    /* get the endpoints and calculate local variables */
    a = 0.0;   /* Left endpoint             */
    b = 1.0;   /* Right endpoint            */
    n = 1024;  /* Number of trapezoids      */
    //a = atof(argv[1]);               /*  left endpoint */
    //b = atof(argv[2]);               /* right endpoint */
    //n = atof(argv[3]);               /* Number of trapezoids */
    h = (b-a)/n;    /* h is the same for all processes */



    local_n = n/p;  /* So is the number of trapezoids */
	
	/* Length of each process' interval of
     * integration = local_n*h.  So my interval
     * starts at: */
    local_a = a + my_rank*local_n*h;
    local_b = local_a + local_n*h;
	
	printf("\na=%f, b=%f, Local number of trapezoids=%d\n", local_a, local_b, local_n);
	
    integral = Trap(local_a, local_b, local_n, h);
	
	/* Add up the integrals calculated by each process */
    if (my_rank == 0) {
        total = integral;
        for (source = 1; source < p; source++) {
            MPI_Recv(&integral, 1, MPI_FLOAT, source, tag,
                MPI_COMM_WORLD, &status);
            total = total + integral;
        }
    } else {  
        MPI_Send(&integral, 1, MPI_FLOAT, dest,
            tag, MPI_COMM_WORLD);
    }

    /* Print the result */
    if (my_rank == 0) {
        gettimeofday(&t2, NULL);  /* Walltime */
 
		elapsed_seconds  = t2.tv_sec  - t1.tv_sec;
		elapsed_useconds = t2.tv_usec - t1.tv_usec;
		elapsed_mtime = ((elapsed_seconds) * 1000 + elapsed_useconds/1000.0) + 0.5;
		
		if (elapsed_useconds < 0) {
			elapsed_useconds = 1000000 + elapsed_useconds;
			elapsed_seconds--;
		}
		printf("With n = %d trapezoids, our estimate\n", n);
		printf("of the integral from %f to %f = %f\n", a, b, total);
		printf("Elapsed time is %ld msec\n", elapsed_mtime);
		printf("Elapsed time is %ld.%.6ld sec\n", elapsed_seconds, elapsed_useconds);
    }

    /* Shut down MPI */
    MPI_Finalize();
} /*  main  */

float Trap(float local_a, float local_b, long int local_n, float h) {
    float integral;   /* Store result in integral  */
    float x;
    int i;

    float f(float x); /* function we're integrating */

    integral = (f(local_a) + f(local_b))/2.0;
    x = local_a;
    for (i = 1; i <= local_n-1; i++) {
        x = x + h;
        integral = integral + f(x);
    }
    integral = integral*h;
    return integral;
} /*  Trap  */

float f(float x) {
    float return_val;
    /* Calculate f(x). */
    /* Store calculation in return_val. */
    return_val = x*x;
    return return_val;
} /* f */
